## Basic EXE file analysis from command line

### Goal:  
Verify whether given file is malicious or if any of it's components   
pose a threat, using nothing but freely available command line tools.

Final result containing **hash** of the file, number of **VirusTotal engines** that identified file as *malicious* and a file **name**:
```
    > vtls
    
    Sha256                                                            VT  File
    1d560a107ff178dde5a362d7bbf1bde01e60a18adec8290be7954fd251533c00  0   ColorConversion.fx
    a5c3e4659cadd2aac9bd99fdc73225368d618121307d24be0616d963a78f3056  -   Install.cmd
    abb40658803a6a9bcf9c153ae4b98757665587a74b8c6e52963c2b1cf0e5de1b  0   Install.exe
    dedf511ec624147d7b9a231b6b7f0e64293927c71f2653a387a136ba9b3d95fd  0   RestartComputer.dll
    06e984742de838a8006fb4a6bd9f4b41385629c9df3d695cff1b57e175755f72  0   SetRegistry64.exe
    86a8c1025af331d520dda245136b4a824a60479eeb70e46ae71efef8e48ff2d7  0   SetRegistry.exe
    a7f5ff63e16d6ecda99c421a66530c3f318c8cbec5c621ee8d94f36108ada06c  -   Uninstall.cmd
    5595f12b7fda7e0816c062fcf28bc50c11784a0bb8f6a15bb2b6e0f21f6310f4  0   Uninstall.exe
    7f09116982870f6de5abda4ceb838c6ad7e631dac982da831fab1df4fa93d291  0   VDD2HookKmode.sys
    a1c7c564cda3005a01645b379e8a54cc9d1404864625a9277401633a7bc180fe  0   VDD2HookKmode.sys$0
    5dde8205a8aeddc36eba19e0e9b3faf9beb73cb3cf299b32ec6756e66121fe3f  0   VDD2HookUmode.dll
    087bf97968146c706a03670f1d8574f3eb15e17f23b29191f642f9069c0c934d  0   VDD2HookUmode.dll$0
    7193bc6996aa37d98720e42a3c6061e335e203c948840799a6191706e8f33eac  -   vdd2netservice.cat
    d923b1747572b1f7ce79872358feb17fdeff420ecdc6001cfdac04ab11c5a533  -   vdd2netservice.cat$0
    e96bb3b7cbe8bd70cdadbd1a1929ee08ae96d4aac175c8dc0c33e570e2d9b285  -   VDD2NetService.inf
    e96bb3b7cbe8bd70cdadbd1a1929ee08ae96d4aac175c8dc0c33e570e2d9b285  -   VDD2NetService.inf$0
    a247d085c4dd92b760c80abb0064b122d891d44fa995f82781fc60263100686c  0   VDD2SetupFileDeleteOnReboot64.exe
    86d77839a350ae9dcefca53065d3ecd7deb34ab782914389d5f64b6e9efc0e98  0   VDD2SetupFileDeleteOnReboot64.exe$0
    60968d5824d957facbb460fa7598fc3d9eca0ea70b6ad28f854d7624600a2653  0   VDD2SetupFileDeleteOnReboot.exe
    aec22dd0f83c8ba50bc790b6414c417b7a61a06d278564f8db94ad6d95fb2f10  1   VDD2SetupFileDeleteOnReboot.exe$0
```

### Requirements

- `machinae` v1.4.8+
    > command line tool for checking arifacts against multiple public databases  
    > URL: https://github.com/HurricaneLabs/machinae
- `binwalk` v2.1+
    > Firmware analysis tool 
- `sha256sum` 
    > A set of basic GNU tools commonly used in shell scripts  
    > Provided by `coreutils-8.31`
- `innoextract` v1.7+
    > Extracts installers created by Inno Setup 1.2.10 to 5.6.0
- `jq`
    > JSON query / transformation / manipulation

### Aliases

Create helpful aliases:

1. `alias check-hash='machinae -o J -q -s vt_hash'`  

    For given hash value check if it is present in VirusTotal data. 

    Explained:  
    - `-s vt_hash` - use VirusTotal Hash check
    - `-o J` - generate JSON output
    - `-q` - be quiet (useful for parsing with `jq`)

2. `alias shals='for file in $(ls -p | grep -v "/" ); do sha256sum $file; done'`

    Calculates sha256 checksum for all files in directory.

    ```bash
    for file in $(ls -p | grep -v "/" );
    do 
        sha256sum $file;
    done
    ```

    Explained:  
    - `ls -p` - adds `/` to all directories in the listing
    - `grep -v "/"` - removes all directories from listing

### Analysis

Suspicious EXE file: `http://www.awindinc.com/winUpdate/VDD2setup.exe`

1. Get file
    ```bash
    > wget "http://www.awindinc.com/winUpdate/VDD2setup.exe"
    --2019-06-17 13:23:44--  http://www.awindinc.com/winUpdate/VDD2setup.exe
    Resolving www.awindinc.com (www.awindinc.com)... 218.211.90.136
    Connecting to www.awindinc.com (www.awindinc.com)|218.211.90.136|:80... connected.
    HTTP request sent, awaiting response... 200 OK
    Length: 1447065 (1.4M) [application/octet-stream]
    Saving to: ‘VDD2setup.exe.2’

    VDD2setup.exe.2       100% [======================>]   1.38M   516KB/s    in 2.7s    

    2019-06-17 13:23:48 (516 KB/s) - ‘VDD2setup.exe.2’ saved [1447065/1447065]
    ```

2. Generate checksum/hash and check agains VT

    ```bash
    > sha256sum VDD2setup.exe
    50fddcdbc9ebe0011fc90bfa592c131088c080db0621a62ccb8befef2dc83baa  VDD2setup.exe
    > check-hash 50fddcdbc9ebe0011fc90bfa592c131088c080db0621a62ccb8befef2dc83baa
    {
    "site": "VirusTotal File Report",
    "results": {
        "Date submitted": "2019-06-14 09:50:31",
        "Detected engines": 0,
        "Total engines": 63
    },
    "observable": "50fddcdbc9ebe0011fc90bfa592c131088c080db0621a62ccb8befef2dc83baa",
    "observable type": "hash.sha256",
    "observable type detected": true
    }
    ```
    Clean so far, but lets make sure there is no surprise down the rabbit hole.

3. Check if file is a valid binary / executable

    ```bash
    > file VDD2setup.exe
    VDD2setup.exe: PE32 executable (GUI) Intel 80386, for MS Windows
    ```

4. Use `binwalk` to indentify components and extract them if possible

    ```bash
    > binwalk -e VDD2setup.exe 

    DECIMAL       HEXADECIMAL     DESCRIPTION
    --------------------------------------------------------------------------------
    0             0x0             Microsoft executable, portable (PE)
    119376        0x1D250         XML document, version: "1.0"
    120372        0x1D634         Unix path: /schemas.microsoft.com/SMI/2005/WindowsSettings">true</dpiAware>

    > ls *
    VDD2setup.exe

    _VDD2setup.exe.extracted:
    1D250.xml
    ```

    Unfortunately, binwalk in this case does not extract much - only single XML file with a lot of code in `assebly` tag. Let's look inside for some clues:

    ![xml](xml.png)

    We can see that this executable was created by something named `JR.Inno.Setup` which, as it turns out, is fairly popular packer.  

    ![setup](setup.png)

    After some research we find that there's a tool `innoextract` that can be used to extract components from this type of executable.
    
5. Use `innoextract` for extraction

    ```bash
    > innoextract --collisions rename VDD2setup.exe 
    Extracting "Virtual Display Driver" - setup data version 5.5.7 (unicode)
    - "app/amd64/ColorConversion.fx"
    - "app/amd64/VDD2HookKmode.sys$0"
    - "app/amd64/VDD2HookUmode.dll$0"
    - "app/x86/ColorConversion.fx"
    - "app/x86/VDD2HookKmode.sys$0"
    - "app/x86/VDD2HookUmode.dll$0"
    - "app/vdd2netservice.cat$0"
    - "app/VDD2NetService.inf$0"
    - "app/VDD2SetupFileDeleteOnReboot.exe$0"
    - "app/VDD2SetupFileDeleteOnReboot64.exe$0"
    - "app/amd64/VDD2HookKmode.sys"
    - "app/amd64/VDD2HookUmode.dll"
    - "app/x86/VDD2HookKmode.sys"
    - "app/x86/VDD2HookUmode.dll"
    - "app/vdd2netservice.cat"
    - "app/VDD2NetService.inf"
    - "app/VDD2SetupFileDeleteOnReboot.exe"
    - "app/VDD2SetupFileDeleteOnReboot64.exe"
    - "app/Install.cmd"
    - "app/Uninstall.cmd"
    - "app/Install.exe"
    - "app/SetRegistry.exe"
    - "app/SetRegistry64.exe"
    - "app/Uninstall.exe"
    - "app/RestartComputer.dll"
    Done.
    ```

    **Note**: `--collisions rename` is necessary if you don't want overwriting files with the same name upon extraction.

    Now we see much more.  
    Multiple files to check - here is where our aliases come in handy.

6. Automate checks

    Get hashes for all files:
    ```bash
    > cd app
    > shals 
    1d560a107ff178dde5a362d7bbf1bde01e60a18adec8290be7954fd251533c00  ColorConversion.fx
    a5c3e4659cadd2aac9bd99fdc73225368d618121307d24be0616d963a78f3056  Install.cmd
    abb40658803a6a9bcf9c153ae4b98757665587a74b8c6e52963c2b1cf0e5de1b  Install.exe
    dedf511ec624147d7b9a231b6b7f0e64293927c71f2653a387a136ba9b3d95fd  RestartComputer.dll
    06e984742de838a8006fb4a6bd9f4b41385629c9df3d695cff1b57e175755f72  SetRegistry64.exe
    86a8c1025af331d520dda245136b4a824a60479eeb70e46ae71efef8e48ff2d7  SetRegistry.exe
    a7f5ff63e16d6ecda99c421a66530c3f318c8cbec5c621ee8d94f36108ada06c  Uninstall.cmd
    5595f12b7fda7e0816c062fcf28bc50c11784a0bb8f6a15bb2b6e0f21f6310f4  Uninstall.exe
    7f09116982870f6de5abda4ceb838c6ad7e631dac982da831fab1df4fa93d291  VDD2HookKmode.sys
    a1c7c564cda3005a01645b379e8a54cc9d1404864625a9277401633a7bc180fe  VDD2HookKmode.sys$0
    5dde8205a8aeddc36eba19e0e9b3faf9beb73cb3cf299b32ec6756e66121fe3f  VDD2HookUmode.dll
    087bf97968146c706a03670f1d8574f3eb15e17f23b29191f642f9069c0c934d  VDD2HookUmode.dll$0
    7193bc6996aa37d98720e42a3c6061e335e203c948840799a6191706e8f33eac  vdd2netservice.cat
    d923b1747572b1f7ce79872358feb17fdeff420ecdc6001cfdac04ab11c5a533  vdd2netservice.cat$0
    e96bb3b7cbe8bd70cdadbd1a1929ee08ae96d4aac175c8dc0c33e570e2d9b285  VDD2NetService.inf
    e96bb3b7cbe8bd70cdadbd1a1929ee08ae96d4aac175c8dc0c33e570e2d9b285  VDD2NetService.inf$0
    a247d085c4dd92b760c80abb0064b122d891d44fa995f82781fc60263100686c  VDD2SetupFileDeleteOnReboot64.exe
    86d77839a350ae9dcefca53065d3ecd7deb34ab782914389d5f64b6e9efc0e98  VDD2SetupFileDeleteOnReboot64.exe$0
    60968d5824d957facbb460fa7598fc3d9eca0ea70b6ad28f854d7624600a2653  VDD2SetupFileDeleteOnReboot.exe
    aec22dd0f83c8ba50bc790b6414c417b7a61a06d278564f8db94ad6d95fb2f10  VDD2SetupFileDeleteOnReboot.exe$0

    ```

    Loop over first column:
    ```bash
    for hash in $(shals | cut -d " " -f 1);
    do
        check_hash $hash
    done
    ```

    Result:
    ```json
    {
        "site": "VirusTotal File Report",
        "results": {},
        "observable": "a5c3e4659cadd2aac9bd99fdc73225368d618121307d24be0616d963a78f3056",
        "observable type": "hash.sha256",
        "observable type detected": true
    }
    {
        "site": "VirusTotal File Report",
        "results": {
            "Date submitted": "2018-03-09 03:45:05",
            "Detected engines": 0,
            "Total engines": 67
        },
        "observable": "abb40658803a6a9bcf9c153ae4b98757665587a74b8c6e52963c2b1cf0e5de1b",
        "observable type": "hash.sha256",
        "observable type detected": true
    }
    
    (...)
    ```
    Output is a little bit too verbose and with redundant elements.  
    Lets fix it.

7. Tailor output

    `jq` is irreplaceable when it comes to handling JSON in command line.

    ```bash
    > {...} | jq -c '{
                        hash:.observable, 
                        date:.results."Date submitted", 
                        detected:.results."Detected engines"
                    }'    
    ```


    Test run result:
    ```json
    {"hash":"a5c3e4659cadd2aac9bd99fdc73225368d618121307d24be0616d963a78f3056","date":null,"detected":null}
    {"hash":"abb40658803a6a9bcf9c153ae4b98757665587a74b8c6e52963c2b1cf0e5de1b","date":"2018-03-09 03:45:05","detected":0}
    {"hash":"dedf511ec624147d7b9a231b6b7f0e64293927c71f2653a387a136ba9b3d95fd","date":"2017-01-08 10:12:43","detected":0}
    {"hash":"06e984742de838a8006fb4a6bd9f4b41385629c9df3d695cff1b57e175755f72","date":"2018-10-04 15:37:39","detected":0}
    {"hash":"86a8c1025af331d520dda245136b4a824a60479eeb70e46ae71efef8e48ff2d7","date":"2018-05-26 21:59:40","detected":0}
    {"hash":"a7f5ff63e16d6ecda99c421a66530c3f318c8cbec5c621ee8d94f36108ada06c","date":null,"detected":null}

    ```

    Still more than I would normally need and not very usefull without filename next to hash value.

8. Combine it all

    ```bash
    #!/bin/bash

    # Print header once
    header=0

    # For all files in directory
    for file in $(ls -p | grep -v "/" ); 
    do
        # Header
        if [[ $header -lt 1 ]];
        then
            echo "Sha256 VT File"
            let header=1
        fi

        # Get hash
        hash=$(sha256sum $file | cut -d " " -f 1);

        # Get results from VT
        vt_result=$(machinae -o J -q -s vt_hash $hash);

        # Minimal confirmation about detection
        detected=$(echo $vt_result | jq -c '.results."Detected engines"')

        # Replace null with hyphen
        if [ "$detected" == "null" ];
        then
            echo "$hash - $file"
        else
            echo "$hash $detected $file"
        fi

        # Use sleep if you use free VT API access
        # Limit is 4 queries per minute
        sleep 15

        # Show result in nice columns
        # Space after \ is required
    done | column -t -s\ 

    ```

    Final result:
    ```
    Sha256                                                            VT  File
    1d560a107ff178dde5a362d7bbf1bde01e60a18adec8290be7954fd251533c00  0   ColorConversion.fx
    a5c3e4659cadd2aac9bd99fdc73225368d618121307d24be0616d963a78f3056  -   Install.cmd
    abb40658803a6a9bcf9c153ae4b98757665587a74b8c6e52963c2b1cf0e5de1b  0   Install.exe
    dedf511ec624147d7b9a231b6b7f0e64293927c71f2653a387a136ba9b3d95fd  0   RestartComputer.dll
    06e984742de838a8006fb4a6bd9f4b41385629c9df3d695cff1b57e175755f72  0   SetRegistry64.exe
    86a8c1025af331d520dda245136b4a824a60479eeb70e46ae71efef8e48ff2d7  0   SetRegistry.exe
    a7f5ff63e16d6ecda99c421a66530c3f318c8cbec5c621ee8d94f36108ada06c  -   Uninstall.cmd
    5595f12b7fda7e0816c062fcf28bc50c11784a0bb8f6a15bb2b6e0f21f6310f4  0   Uninstall.exe
    7f09116982870f6de5abda4ceb838c6ad7e631dac982da831fab1df4fa93d291  0   VDD2HookKmode.sys
    a1c7c564cda3005a01645b379e8a54cc9d1404864625a9277401633a7bc180fe  0   VDD2HookKmode.sys$0
    5dde8205a8aeddc36eba19e0e9b3faf9beb73cb3cf299b32ec6756e66121fe3f  0   VDD2HookUmode.dll
    087bf97968146c706a03670f1d8574f3eb15e17f23b29191f642f9069c0c934d  0   VDD2HookUmode.dll$0
    7193bc6996aa37d98720e42a3c6061e335e203c948840799a6191706e8f33eac  -   vdd2netservice.cat
    d923b1747572b1f7ce79872358feb17fdeff420ecdc6001cfdac04ab11c5a533  -   vdd2netservice.cat$0
    e96bb3b7cbe8bd70cdadbd1a1929ee08ae96d4aac175c8dc0c33e570e2d9b285  -   VDD2NetService.inf
    e96bb3b7cbe8bd70cdadbd1a1929ee08ae96d4aac175c8dc0c33e570e2d9b285  -   VDD2NetService.inf$0
    a247d085c4dd92b760c80abb0064b122d891d44fa995f82781fc60263100686c  0   VDD2SetupFileDeleteOnReboot64.exe
    86d77839a350ae9dcefca53065d3ecd7deb34ab782914389d5f64b6e9efc0e98  0   VDD2SetupFileDeleteOnReboot64.exe$0
    60968d5824d957facbb460fa7598fc3d9eca0ea70b6ad28f854d7624600a2653  0   VDD2SetupFileDeleteOnReboot.exe
    aec22dd0f83c8ba50bc790b6414c417b7a61a06d278564f8db94ad6d95fb2f10  1   VDD2SetupFileDeleteOnReboot.exe$0
    ```

    ..and we have a hit.  
    Seems like file `aec22dd0f83c8ba50bc790b6414c417b7a61a06d278564f8db94ad6d95fb2f10` is recognized as malicious by one of the VT engines:

    ![vt](vt.png)

    Is it False Positive? Most probably yes, but to make sure we have to dive deeper.  
    Maybe next time though ;)


---
Happy Hunting  
mer
