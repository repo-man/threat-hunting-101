#!/bin/bash

# Print header once
header=0

# For all files in directory
for file in $(ls -p | grep -v "/" ); 
do
    # Header
    if [[ $header -lt 1 ]];
    then
        echo "Sha256 VT File"
        let header=1
    fi

    # Get hash
    hash=$(sha256sum $file | cut -d " " -f 1);

    # Get results from VT
    vt_result=$(machinae -o J -q -s vt_hash $hash);

    # Minimal confirmation about detection
    detected=$(echo $vt_result | jq -c '.results."Detected engines"')

    # Replace null with hyphen
    if [ "$detected" == "null" ];
    then
        echo "$hash - $file"
    else
        echo "$hash $detected $file"
    fi

    # Use sleep if you use free VT API access
    # Limit is 4 queries per minute
    sleep 15

    # Show result in nice columns
    # Space after \ is required
done | column -t -s\ 
